Eine sehr simple Website zur Vorstellung verschiedener Lederwaren. Implementiert mit einer angepassten Bootstrap-Vorlage.
Live-Version: https://collie.gitlab.io/lederwaren

## Technologien

- Bootstrap
- HTML/SASS/JS

## Reflektion

Eins meiner ersten Webprojekte. Auch nach einem Jahr bin ich mit dem Layout zufrieden, würde aber vermutlich die Seite komplett anders umsetzen. Je nach Produktmenge und Vielfalt sollte man auf jeden Fall mit einer Templating Engine und evt. einem Backend arbeiten. Für die bisherige Nutzung als alleinige Seite zum Vorzeigen der Produktpalette ausreichend.

Die Seite hat noch ein paar kleine Macken, allerdings wird sie aktuell nicht weiterentwickelt, da die Betreiber der Werkstatt keine Waren mehr herstellen.
